#include <nuttx/config.h>

#include <stdio.h>
// this is in drivers
#include <esp_idf_port/include/esp_wifi.h>
#include <esp_idf_port/include/esp_wifi_types.h>
#include <esp_idf_port/include/esp_event.h>
#include <esp_idf_port/include/esp_event_loop.h>


#define CONFIG_ESP32_WIFI_STATIC_RX_BUFFER_NUM 128  /// some size to be checked...
#define CONFIG_ESP32_WIFI_DYNAMIC_RX_BUFFER_NUM 128 /// some size to be checked...
#define CONFIG_ESP32_WIFI_TX_BUFFER_TYPE 1          /// whatever that means

esp_err_t esp_event_send(system_event_t *event);
extern const wpa_crypto_funcs_t g_wifi_default_wpa_crypto_funcs;

wifi_init_config_t cfg = {
    .event_handler = &esp_event_send,
    .osi_funcs = &g_wifi_osi_funcs,
    .wpa_crypto_funcs = {},
    //.wpa_crypto_funcs = g_wifi_default_wpa_crypto_funcs,
    .static_rx_buf_num = CONFIG_ESP32_WIFI_STATIC_RX_BUFFER_NUM,
    .dynamic_rx_buf_num = CONFIG_ESP32_WIFI_DYNAMIC_RX_BUFFER_NUM,
    .tx_buf_type = CONFIG_ESP32_WIFI_TX_BUFFER_TYPE,
    .static_tx_buf_num = WIFI_STATIC_TX_BUFFER_NUM,
    .dynamic_tx_buf_num = WIFI_DYNAMIC_TX_BUFFER_NUM,
    .csi_enable = WIFI_CSI_ENABLED,
    .ampdu_rx_enable = WIFI_AMPDU_RX_ENABLED,
    .ampdu_tx_enable = WIFI_AMPDU_TX_ENABLED,
    .nvs_enable = WIFI_NVS_ENABLED,
    .nano_enable = WIFI_NANO_FORMAT_ENABLED,
    .tx_ba_win = WIFI_DEFAULT_TX_BA_WIN,
    .rx_ba_win = WIFI_DEFAULT_RX_BA_WIN,
    .wifi_task_core_id = WIFI_TASK_CORE_ID,
    .beacon_max_len = WIFI_SOFTAP_BEACON_MAX_LEN, 
    .magic = WIFI_INIT_CONFIG_MAGIC
};

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    // TODO just print
    return 0;
}

// dunno why there is need for both - I messed up sth with Makefile/Make.defs

int esp_main(int argc, char *argv[])
{
    printf("My test\n");
    int ret=0;

    // -> real added code
    tcpip_adapter_init();
    ret = esp_event_loop_init(event_handler, NULL);
    // <- real added code

    // library usage
    ret = esp_wifi_init(&cfg);

    // from wifi_utils <3
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = "test",
            .password = "test",
            .scan_method = WIFI_FAST_SCAN,
            .sort_method = WIFI_CONNECT_AP_BY_SIGNAL,
            .threshold.rssi = -127,
            .threshold.authmode = WIFI_AUTH_OPEN,
        }
        ,
    };
    ret = esp_wifi_set_mode(WIFI_MODE_STA);
    if (ret) {
        printf("esp_wifi_set_mode failed, %d\n", ret);
        return -1;
    }

    ret = esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config);
    if (ret) {
        printf("esp_wifi_set_config failed, %d\n", ret);
        return -1;
    }

    ret = esp_wifi_start();
    if (ret != ESP_OK) {
        printf("esp_wifi_start failed\n");
        return -1;
    }
    return 0;
}

int esp_main_main(int argc, char *argv[])
{
    printf("My test 2\n");
    return 0;
}

